#pragma once

#include "definitions.h"

#include <mpi.h>
#include <string>
#include <vector>

struct Particle {
    real2 r;
    real2 v;
};

struct Force {
    real2 f;
};

std::vector<Particle> initializeRandomUniform(size_t n, const Domain& domain,
                                              long seed = 42);
std::vector<Particle> initializeGridUniform(size_t n, const Domain& domain,
                                            real mass, real kBT,
                                            long seed = 42);

void dumpCSV(const std::vector<Particle>& particles, MPI_Comm comm,
             const std::string& filename, const Domain& domain);
void dumpXYZ(const std::vector<Particle>& particles, MPI_Comm comm,
             const std::string& filename, const Domain& domain);
