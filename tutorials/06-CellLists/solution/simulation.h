#pragma once

#include "celllists.h"
#include "exchangers.h"
#include "interactions.h"
#include "particles.h"
#include "utils.h"

#include <cstdio>
#include <mpi.h>

class Simulation
{
  public:
    Simulation(MPI_Comm comm, int2 nRanks2D, const Domain& domain,
               LennardJones interaction, real mass, real kBT, int n);

    void advance(real dt);
    void printStats(FILE* stream);
    void dumpCsv(const std::string& filename) const;
    void dumpXyz(const std::string& filename) const;

  private:
    void clearForces();
    void computeBulk();
    void computeHalo();
    void velocityVerlet(real dt);

  private:
    MPI_Comm comm;
    int rank;

    Domain domain;

    Redistributor redistributor;
    GhostExchanger ghostExchanger;

    CellLists cl;
    std::vector<Particle> particles, ghosts;
    std::vector<Force> forces;
    LennardJones interaction;

    real mass;
    real time{0.0_r};
};
