#include <cmath>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <random>
#include <sstream>
#include <vector>

struct Particle {
    double x, y, z;
    double r;
    int rank;
};

std::vector<Particle> get_particles(int rank, int commsize, int N, int t)
{
    std::default_random_engine gen;
    std::normal_distribution<double> normal(0, 0.1);

    std::vector<Particle> pp;
    // helix
    for (int i = rank * N / commsize; i < (rank + 1) * N / commsize; ++i) {
        double zc = 4 * (double(i) / N);
        double phi = 2 * M_PI * (zc + t * 0.1);
        double xc = std::cos(phi);
        double yc = std::sin(phi);
        double x = xc + normal(gen);
        double y = yc + normal(gen);
        double z = zc + normal(gen);
        double r = std::sqrt(x * x + y * y);
        pp.push_back({x, y, z, r, rank});
    }
    return pp;
}

std::string get_csv_header()
{
    return "x,y,z,r,rank";
}

std::string get_csv_particles(const std::vector<Particle>& pp)
{
    std::stringstream s;
    for (auto& p : pp) {
        s << p.x << ',';
        s << p.y << ',';
        s << p.z << ',';
        s << p.r << ',';
        s << p.rank << '\n';
    }
    return s.str();
}

void write_gather(std::string path, const std::vector<Particle>& pp,
                  MPI_Comm comm, int rank)
{
    int commsize;
    MPI_Comm_size(comm, &commsize);

    std::string csv = get_csv_particles(pp);
    int cnt = csv.size();

    if (rank == 0) {
        std::vector<int> cnts(commsize);
        MPI_Gather(&cnt, 1, MPI_INT, cnts.data(), 1, MPI_INT, 0, comm);

        std::vector<int> displs(commsize + 1);
        displs[0] = 0;
        for (int i = 0; i < commsize; ++i) {
            displs[i + 1] = displs[i] + cnts[i];
        }

        std::vector<char> buf(displs[commsize]);
        MPI_Gatherv(csv.data(), cnt, MPI_CHAR, buf.data(), cnts.data(),
                    displs.data(), MPI_CHAR, 0, comm);

        std::ofstream fout(path);
        fout << get_csv_header() << std::endl;
        for (auto c : buf) {
            fout << c;
        }
    } else {
        MPI_Gather(&cnt, 1, MPI_INT, nullptr, 0, MPI_INT, 0, comm);
        MPI_Gatherv(csv.data(), cnt, MPI_CHAR, nullptr, nullptr, nullptr,
                    MPI_CHAR, 0, comm);
    }
}

void write_mpi(std::string path, const std::vector<Particle>& pp, MPI_Comm comm,
               int rank)
{
    MPI_File fout;
    MPI_File_open(comm, path.data(), MPI_MODE_CREATE | MPI_MODE_WRONLY,
                  MPI_INFO_NULL, &fout);

    if (rank == 0) {
        auto h = get_csv_header() + '\n';
        MPI_File_write_shared(fout, h.data(), h.size(), MPI_CHAR,
                              MPI_STATUS_IGNORE);
    }

    std::string csv = get_csv_particles(pp);
    MPI_File_write_ordered(fout, csv.data(), csv.size(), MPI_CHAR,
                           MPI_STATUSES_IGNORE);
    MPI_File_close(&fout);
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm comm = MPI_COMM_WORLD;

    int commsize, rank;
    MPI_Comm_size(comm, &commsize);
    MPI_Comm_rank(comm, &rank);

    const int N = 5000;
    const int tmax = 9;
    for (int t = 0; t <= tmax; ++t) {
        if (rank == 0) {
            std::cout << "t=" << t << std::endl;
        }
        const auto pp = get_particles(rank, commsize, N, t);
        const auto st = std::to_string(t);
        write_gather("part_" + st + ".csv", pp, comm, rank);
        write_mpi("partmpi_" + st + ".csv", pp, comm, rank);
    }

    MPI_Finalize();
}
